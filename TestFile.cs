using System;
using System.Linq;
class Example
{
    static void Main()
    {
        int[] n1 = new int[12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
        var nQuery =
            from VrNum in n1
            where (VrNum % 3) == 0
            select VrNum;
        foreach (int VrNum in nQuery)
        {
            Console.Write("{0} ", VrNum);
        }
        Console.Write("\n\n");
    }
}