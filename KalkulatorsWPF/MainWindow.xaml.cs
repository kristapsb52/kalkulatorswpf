﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace KalkulatorsWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<int> fibNumbers = new List<int>();
        int fibCompletedIterations = 1;
        int iterations = 20;
        Boolean started = false;

        float operand1 = 0;
        float operand2 = 0;
        string operation = "";
        Boolean equalPressed = false;
        Boolean dotPressed = false;
        private int ticks = 0;
        private DispatcherTimer dispatcherTimer = new DispatcherTimer();


        public MainWindow()
        {
            InitializeComponent();
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Tick += dispatcherTimer_Tick;
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            int tempResult = fibNumbers[fibCompletedIterations] + fibNumbers[fibCompletedIterations - 1];
            fibNumbers.Add(tempResult);
            string fibResult = Convert.ToString(tempResult);
            FibResult.Text = fibResult;

            fibCompletedIterations += 1;
            ticks += 1;
            progressSlider.Value = fibCompletedIterations;
            if(fibCompletedIterations == iterations)
            {
                dispatcherTimer.Stop();
            }
        }
            private void Digit7_click(object sender, RoutedEventArgs e)
        {
            //23
            //231 = 23*10+1
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    if(dotPressed == true)
                    {
                        string temp = operand1.ToString();
                        temp = temp + ".7";
                        operand1 = float.Parse(temp);
                    }
                    else
                    {
                        operand1 = (operand1 * 10) + 7;
                    }
                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                if (dotPressed == true)
                {
                    string temp = operand2.ToString();
                    temp = temp + ".7";
                    operand2 = float.Parse(temp);
                }
                else
                {
                    operand2 = (operand2 * 10) + 7;
                }
                Text_box.Text = operand2.ToString();
            }

        }

        private void Digit1_click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    if (dotPressed == true)
                    {
                        string temp = operand1.ToString();
                        temp = temp + ".1";
                        operand1 = float.Parse(temp);
                    }
                    else
                    {
                        operand1 = (operand1 * 10) + 1;
                    }
                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                if (dotPressed == true)
                {
                    string temp = operand2.ToString();
                    temp = temp + ".1";
                    operand2 = float.Parse(temp);
                }
                else
                {
                    operand2 = (operand2 * 10) + 1;
                }
                Text_box.Text = operand2.ToString();
            }
        }

        private void Digit2_click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    if (dotPressed == true)
                    {
                        string temp = operand1.ToString();
                        temp = temp + ".2";
                        operand1 = float.Parse(temp);
                    }
                    else
                    {
                        operand1 = (operand1 * 10) +2;
                    }
                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                if (dotPressed == true)
                {
                    string temp = operand2.ToString();
                    temp = temp + ".2";
                    operand2 = float.Parse(temp);
                }
                else
                {
                    operand2 = (operand2 * 10) + 2;
                }
                Text_box.Text = operand2.ToString();
            }
        }

        private void Digit3_click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    if (dotPressed == true)
                    {
                        string temp = operand1.ToString();
                        temp = temp + ".3";
                        operand1 = float.Parse(temp);
                    }
                    else
                    {
                        operand1 = (operand1 * 10) + 3;
                    }
                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                if (dotPressed == true)
                {
                    string temp = operand2.ToString();
                    temp = temp + ".3";
                    operand2 = float.Parse(temp);
                }
                else
                {
                    operand2 = (operand2 * 10) + 3;
                }
                Text_box.Text = operand2.ToString();
            }
        }

        private void Digit4_click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    if (dotPressed == true)
                    {
                        string temp = operand1.ToString();
                        temp = temp + ".4";
                        operand1 = float.Parse(temp);
                    }
                    else
                    {
                        operand1 = (operand1 * 10) + 4;
                    }
                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                if (dotPressed == true)
                {
                    string temp = operand2.ToString();
                    temp = temp + ".4";
                    operand2 = float.Parse(temp);
                }
                else
                {
                    operand2 = (operand2 * 10) + 4;
                }
                Text_box.Text = operand2.ToString();
            }
        }

        private void Digit5_click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    if (dotPressed == true)
                    {
                        string temp = operand1.ToString();
                        temp = temp + ".5";
                        operand1 = float.Parse(temp);
                    }
                    else
                    {
                        operand1 = (operand1 * 10) + 5;
                    }
                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                if (dotPressed == true)
                {
                    string temp = operand2.ToString();
                    temp = temp + ".5";
                    operand2 = float.Parse(temp);
                }
                else
                {
                    operand2 = (operand2 * 10) + 5;
                }
                Text_box.Text = operand2.ToString();
            }
        }

        private void Digit6_click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    if (dotPressed == true)
                    {
                        string temp = operand1.ToString();
                        temp = temp + ".6";
                        operand1 = float.Parse(temp);
                    }
                    else
                    {
                        operand1 = (operand1 * 10) + 6;
                    }
                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                if (dotPressed == true)
                {
                    string temp = operand2.ToString();
                    temp = temp + ".6";
                    operand2 = float.Parse(temp);
                }
                else
                {
                    operand2 = (operand2 * 10) + 6;
                }
                Text_box.Text = operand2.ToString();
            }
        }

        private void Digit8_click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    if (dotPressed == true)
                    {
                        string temp = operand1.ToString();
                        temp = temp + ".8";
                        operand1 = float.Parse(temp);
                    }
                    else
                    {
                        operand1 = (operand1 * 10) + 8;
                    }
                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                if (dotPressed == true)
                {
                    string temp = operand2.ToString();
                    temp = temp + ".8";
                    operand2 = float.Parse(temp);
                }
                else
                {
                    operand2 = (operand2 * 10) + 8;
                }
                Text_box.Text = operand2.ToString();
            }
        }

        private void Digit9_click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    if (dotPressed == true)
                    {
                        string temp = operand1.ToString();
                        temp = temp + ".9";
                        operand1 = float.Parse(temp);
                    }
                    else
                    {
                        operand1 = (operand1 * 10) + 9;
                    }
                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                if (dotPressed == true)
                {
                    string temp = operand2.ToString();
                    temp = temp + ".9";
                    operand2 = float.Parse(temp);
                }
                else
                {
                    operand2 = (operand2 * 10) + 9;
                }
                Text_box.Text = operand2.ToString();
            }
        }

        private void Digit_0_Click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    operand1 = (operand1 * 10) + 0;
                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                operand2 = (operand2 * 10) + 0;
                Text_box.Text = operand2.ToString();
            }
        }

        private void ClearButton_C_Click(object sender, RoutedEventArgs e)
        {
            operand1 = 0;
            operand2 = 0;
            Text_box.Text = null;
            equalPressed = false;
            dotPressed = false;
        }

        private void ClearButton_CE_Click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                operand1 = 0;
                Text_box.Text = "0";
                dotPressed = false;
            }
            else
            {
                operand2 = 0;
                Text_box.Text = "0";
                dotPressed = false;
            }

        }

        private void Button_positive_negative_Click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if (equalPressed == false)
                {
                    string temp = operand1.ToString();
                    if (temp[0] != '-')
                    {
                        temp = temp.Insert(0, "-");
                    }
                    else
                    {
                        temp = temp.Remove(0, 1);
                        temp = temp.Insert(0, "+");
                    }
                    operand1 = float.Parse(temp);

                    Text_box.Text = operand1.ToString();
                }
            }
            else
            {
                string temp = operand2.ToString();
                if (temp[0] != '-')
                {
                    temp = temp.Insert(0, "-");
                }
                else
                {
                    temp = temp.Remove(0, 1);
                    temp = temp.Insert(0, "+");
                }
                operand2 = float.Parse(temp);

                Text_box.Text = operand2.ToString();
            }
        }

        private void MathButton_Plus_Click(object sender, RoutedEventArgs e)
        {
            operation = "+";
            Text_box.Text = "0";
            dotPressed = false;
        }

        private void MathButton_Minus_Click(object sender, RoutedEventArgs e)
        {
            operation = "-";
            Text_box.Text = "0";
            dotPressed = false;
        }

        private void MathButton_Times_Click(object sender, RoutedEventArgs e)
        {
            operation = "*";
            Text_box.Text = "0";
            dotPressed = false;
        }

        private void MathButton_Divide_Click(object sender, RoutedEventArgs e)
        {
            operation = "/";
            Text_box.Text = "0";
            dotPressed = false;
        }

        private void MathButton_Equals_Click(object sender, RoutedEventArgs e)
        {

            //TODO Switch cases
            float result = 0;
            if (operation == "+")
            {
                result = operand1 + operand2;
            }
            else if (operation == "-")
            {
                result = operand1 - operand2;
            }
            else if (operation == "*")
            {
                result = operand1 * operand2;
            }
            else if (operation == "/")
            {
                result = operand1 / operand2;
            }
            Text_box.Text = result.ToString();
            operand1 = result;
            operand2 = 0;
            operation = "";
            equalPressed = true;
            dotPressed = false;
        }

        private void ClearButton_Previous_Click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                string temp = operand1.ToString();
                temp = temp.Remove(temp.Length - 1, 1);
                if (temp.Length == 0)
                {
                    operand1 = 0;
                }
                else
                {
                    operand1 = float.Parse(temp);
                }

            }
            else
            {
                string temp = operand2.ToString();
                temp = temp.Remove(temp.Length - 1, 1);
                if (temp.Length == 0)
                {
                    operand2 = 0;
                }
                else
                {
                    operand2 = float.Parse(temp);
                }
            }
            Text_box.Text = Text_box.Text.Remove(Text_box.Text.Length - 1);
        }

        private void Button_dot_Click(object sender, RoutedEventArgs e)
        {
            if (operation == "")
            {
                if(operand1 != 0)
                {
                    if (dotPressed == false)
                    {
                        Text_box.Text = Text_box.Text + ".";
                        dotPressed = true;
                    }
                }
                
            }
            else
            {
                if(operand2 != 0)
                {
                    if (dotPressed == false)
                    {
                        Text_box.Text = Text_box.Text + ".";
                        dotPressed = true;
                    }
                }
            }
        }

        private void Fibonaci_click_start(object sender, RoutedEventArgs e)
        {
            progressSlider.Maximum = iterations;
            fibNumbers.Add(0);
            fibNumbers.Add(1);
            fibCompletedIterations = 1;
            //Starts the iterations
            dispatcherTimer.Start();
            
        }

        private void Fibonaci_click_stop(object sender, RoutedEventArgs e)
        {
            if (dispatcherTimer.IsEnabled)
            {
                dispatcherTimer.Stop();
            }
            else
            {
                progressSlider.Value = 0;
                FibResult.Text = "";
                ticks = 0;
            }
        }

        private void fibIterations_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (fibIterations.Text != "")
                {
                    iterations = Convert.ToInt32(fibIterations.Text);
                }
            } catch
            {
                string message = "Iterations must only contain numbers!";
                string title = "Error";
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
                fibIterations.Text = "0";
            }
        }
    }
}
